# CONNECT W4RS
Connect Wars game instructions and ruleset.

This game is a mutation of connect four and checkers, it was created by Patrick Elumba.
Changes will require a minimum number of approvals for implementation.

## Contents
 * Standard size checker board
 * One set of 21 yellow checkers
 * One set of 21 red checkers
 * (Optional) Various chess pieces
     * Rook
     * Bishop
     * King
     * Queen
     * Others

## Assembly
No assembly is required.

## Set-Up
Place the checker board on a secure flat surface.
Pick a colour - yellow or red. Take all of the checkers of that colour, and give the other coloured checkers to your opponent.
Place the power tokens (chess pieces) next to the board, these chess pieces will remain by the side of the checker board to be obtained during play.

White Rook: Three-in-a-row token
White Bishop: Square token
White Queen: Rotate token
Black King: Stack token

## Objective
Be the first player to get to four or more points. Using your coloured checkers to create multiple links of four in a row - horizontally, vertically or diagonally.
 * A 'natural' link of 4 checkers results in 2 points.
 * An 'assisted' link of 4 checkers results in 1 point. Assisted is determined if the player uses a power play or inventory play to obtain the link.
 
At the end of the 2 games, the player with the most points in total will be deemed the winner.

## How to Play
1. Decide who plays first. Players will alternate turns after playing a checker.
2. Decide which direction of the board is 'down', this will establish the initial direction of gravity for the game.
3. Imagining the board in a vertical position, place one of your checkers in a top-down movement (like Connect Four) in any column until it is stopped by either the bottom row or an existing checker.
4. Play alternates until one player gets two sets of 4 checkers of his/her colour in a row. The 4-in-a-row's can be horizontal, vertical or diagonal. A 4-in-a-row can only share one common checker with another 4-in-a-row.
5. If you're the first player to get two sets of 4 checkers in a row. You win the game.
6. Additional power plays can be played during the game, these are outlined in the following section.

## Inventory
Players have one inventory piece to be in lieu of their turn. The player can choose to use one of the following plays outlined below:
 * Bide: Force the opponent to play their next turn, then the player can play two consecutive turns in a row. Note that the second checker is considered a power play.
 * Destroy: Remove the existing power piece from the opponents possession. If the opponent does not have a power piece, this play cannot be used.

## Power Plays
A power play allows the player to perform an irregular move depending on the power they wield. Power plays are obtained when a player creates a pre-defined pattern with their checkers of choice.

 * A player can only wield one type of power play at a time.
 * The execution of a power play is determined as the players turn, regardless if a checker was placed on the board or not. A player can choose to not utilise the power move and save it for a later turn.
 * If the players opponent creates the same pattern before the player has utilised the power play, the opponent now wields that power.
 * Once a power play is executed, the player no longer has the abilities of that power play unless they create another pre-defined pattern.
 * A pre-defined pattern can only share one common checker from a previously obtained pattern.
 * A player can as many 4-in-a-row links with power plays as they wish.
 * If a player creates multiple patterns with a single move, the player has the option to select only one power move.
 * A power play that creates a new pattern allows the player to obtain the new power play piece.
 * In the rare case that a Power Play results in TWO players creating both their final 4-in-a-row combination to reach 4 points, the opponent does not receive a point.

## Patterns
#### Three-in-a-row
**Pattern:**
```
-------  ---*---  ----*--
--***--  ---*---  ---*---
-------  ---*---  --*----
```

**Power:** A player can insert into in a linear movement the board from any side, ignoring the gravity of the game.

**Notes:** The three-in-a-row can be horizontal, vertical or diagonal.

#### Square
**Pattern:**
```
------  --*---
--**--  -*-*--
--**--  --*---
------  ------
```

**Power:** A player can insert into the board in a diagonal movement from any side of the board, ignoring the gravity of the game.

**Notes:** Square can also be diagonal.

#### Stack
**Pattern:**
```
---*---
--*-*--
--*-*--
---*---
```
**Power:** A player can insert a checker onto any position of the board, including any occupied position. A player cannot take away an opponents existing 4-in-a-row combination.

**Notes:** The pattern can be horizontal, vertical or diagonal.

#### Rotate
**Pattern:**
```
-------  -------
-----*-  -*-----
----**-  -**----
---***-  -***---
```
**Power:** A player can use their turn to rotate the board, creating a new direction of gravity.

**Notes:** 

## Weekly competition tournament
Each week all players that wish to participate enter into a elimination tournament. Tournament rules are bound ONLY to what is written in this instruction set and no other additional rules.
The winner of each round is determined by the player that wins with the most points after 2 consecutive games. Should there be a tie, an additional game is to be played to determine the winner. Before the
third round begins, another round of Scissors Paper Rock is played to determine who plays first. The winner of the Scissors Paper Rock can choose who plays first.

The winner (coined 'The President') of the tournament will have an extra vote for further changes to the game rules. 'The President' cannot maintain their status for more than two terms.
Should they win the tournament for a third time in a row, the player that comes second will take the title.

As a bonus, the president is exempt from contributing to the weekly treats purchased for the tournament.

## Contributors
* Patrick Elumba (Founder)
* Julio Galves
* James Fuller
* Blake McLeod
